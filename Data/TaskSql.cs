
using Microsoft.Data.SqlClient;

public class TaskSql : Database, ITaskData
{
    public void Create(Kanban.Models.Task task)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "INSERT INTO Task VALUES (@title, @description, @points, @priority, @statusid)";

        cmd.Parameters.AddWithValue("@title", task.Title);
        cmd.Parameters.AddWithValue("@description", task.Description);
        cmd.Parameters.AddWithValue("@points", task.Points);
        cmd.Parameters.AddWithValue("@priority", task.Priority);
        cmd.Parameters.AddWithValue("@statusid", task.StatusId);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "DELETE FROM Task WHERE TaskId = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();
    }

    public List<Kanban.Models.Task> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "SELECT * FROM TaskStatus";

        SqlDataReader reader = cmd.ExecuteReader();

        List<Kanban.Models.Task> lista = new();

        while(reader.Read())
        {
            Kanban.Models.Task status = new Kanban.Models.Task();
            status.TaskId = reader.GetInt32(0);
            status.Title = reader.GetString(1);
            status.Description = reader.GetString(2);
            status.Points = reader.GetInt32(3);
            status.Priority = reader.GetString(4);
            status.StatusId = reader.GetInt32(5);
            status.Status = new Status { Name = reader.GetString(6)};

            lista.Add(status);
        }
        return lista;
    }

    
    public Kanban.Models.Task Read(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "SELECT * FROM Task WHERE TaskId = @id";

        // EVITA SQL INJECTION!
        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read())
        {
            Kanban.Models.Task task = new Kanban.Models.Task();
            task.TaskId = reader.GetInt32(0);
            task.Title = reader.GetString(1);
            task.Description = reader.GetString(2);
            task.Points = reader.GetInt32(3);
            task.Priority = reader.GetString(4);
            task.StatusId = reader.GetInt32(5);

            return task;
        }

        return null;
    }

    public void Update(int id, Kanban.Models.Task task)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = @"Update Task SET Title = @title,
        Description = @description, Points =  @points, Priority = @priority,
        StatusId = @statusid WHERE TaskId = @id";

        cmd.Parameters.AddWithValue("@title", task.Title);
        cmd.Parameters.AddWithValue("@description", task.Description);
        cmd.Parameters.AddWithValue("@points", task.Points);
        cmd.Parameters.AddWithValue("@priority", task.Priority);
        cmd.Parameters.AddWithValue("@statusid", task.StatusId);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();
    }
}