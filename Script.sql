CREATE DATABASE Kanban
GO

USE Kanban
GO

CREATE TABLE Status
(
	StatusId  int           primary key  identity,
	Name      varchar(60)   not null
)

INSERT INTO Status VALUES ('To Do')
INSERT INTO Status VALUES ('Doing')
INSERT INTO Status VALUES ('Done')
INSERT INTO Status VALUES ('Cancelled')
INSERT INTO Status VALUES ('Fatec')

SELECT * FROM Status

CREATE TABLE Task
(
	TaskId		int				primary key		identity,
	Title		varchar(100)	not null,
	Description varchar(100)	not null,
	Points		int				not null		default 0,
	Priority	varchar(20)		not null,
	StatusId	int				not null		references Status (StatusId)
)

SELECT * FROM Task

CREATE VIEW TaskStatus
AS
	SELECT Task.*, Status.Name
	FROM Task
	INNER JOIN Status ON Status.StatusId = Task.StatusId

SELECT * FROM TaskStatus